const { MethodNotAllowedError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

const { checkMethod, checkAndSendMethod } = require('.');

let res;
beforeEach(() => {
  res = {
    append: jest.fn(() => res),
    json: jest.fn(() => res),
    status: jest.fn(() => res),
  };
});

describe('checkMethod()', () => {
  test('true when method is allowed', () => {
    expect(checkMethod({ method: 'POST' }, ['POST']))
      .toBe(true);
  });
  test('false when method is not allowed', () => {
    expect(checkMethod({ method: 'POST' }, ['GET']))
      .toBe(false);
  });
  test('true when OPTIONS override', () => {
    expect(checkMethod({ method: 'OPTIONS' }, []))
      .toBe(true);
  });
  test('false when no OPTIONS override', () => {
    expect(checkMethod({ method: 'OPTIONS' }, [], false))
      .toBe(false);
  });
});

describe('checkAndSendMethod()', () => {
  for (const { allowed, method } of [
    { allowed: ['POST'], method: 'GET' },
    { allowed: ['GET'], method: 'POST' },
    { allowed: ['GET', 'POST'], method: 'PUT' },
  ]) {
    test(`HTTP 405 when ${method} but only ${allowed} allowed`, () => {
      const next = jest.fn();
      expect(() => {
        try {
          checkAndSendMethod(allowed)({ method }, res, next);
        } catch (err) {
          expect(err.status).toBe(405);
          throw err;
        }
      }).toThrowError(MethodNotAllowedError);
      expect(next).not.toHaveBeenCalled();
    });
  }
  test('HTTP 405 when OPTIONS and options turned off', () => {
    const next = jest.fn();
    expect(() => {
      try {
        checkAndSendMethod([], false)({ method: 'OPTIONS' }, res, next);
      } catch (err) {
        expect(err.status).toBe(405);
        throw err;
      }
    }).toThrowError(MethodNotAllowedError);
    expect(next).not.toHaveBeenCalled();
  });
  for (const { allowed, method } of [
    { allowed: ['GET'], method: 'GET' },
    { allowed: ['POST'], method: 'POST' },
    { allowed: ['GET', 'POST'], method: 'GET' },
    { allowed: ['GET', 'POST'], method: 'POST' },
    { allowed: ['GET'], method: 'OPTIONS' },
  ]) {
    test(`Passes to next when ${method} and ${allowed} allowed`, () => {
      const next = jest.fn();
      checkAndSendMethod(allowed)({ method }, res, next);
      expect(next).toHaveBeenCalled();
    });
  }
});
