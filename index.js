const _ = require('lodash');

const { MethodNotAllowedError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

const getAllowedMethods = (allowed, options = true) => _.uniq(
  options
    ? [...allowed, 'OPTIONS']
    : allowed,
);

exports.checkMethod = (req, allowed, options) => {
  allowed = getAllowedMethods(allowed, options);
  return allowed.indexOf(req.method) >= 0;
};
exports.checkAndSendMethod = (allowed, options) => function checkAndSendMethod(req, res, next) {
  if (!exports.checkMethod(req, allowed, options)) {
    throw new MethodNotAllowedError(
      `Only ${getAllowedMethods(allowed, options).join(', ')} allowed`,
    );
  }
  return next();
};
